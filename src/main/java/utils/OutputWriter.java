
package utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import model.ObjetMetierSortie;

public class OutputWriter {
    public static void genereOutput(final List<ObjetMetierSortie> listeSortie, final String fileName)
            throws IOException {

        final BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        // Ligne 1 : nbObject
        writer.write(String.valueOf(listeSortie.size()));
        writer.write("\n");
        // Lignes suivantes : contenu de sortie
        for (final ObjetMetierSortie sortie : listeSortie) {
            writer.write(sortie.toString());
        }

        writer.close();
    }
}
