
package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import model.ObjetMetier;

public class InputReader {
    public static List<ObjetMetier> getObjetsMetiersFromFile(final String path)
            throws URISyntaxException, FileNotFoundException {
        final List<ObjetMetier> listeObjetsMetier = new ArrayList<ObjetMetier>();
        final URL url = InputReader.class.getResource("../" + path);
        System.out.println(url);

        final File file = new File(url.toURI());
        final Scanner in = new Scanner(file);
        int idObject = 0;

        while (in.hasNextLine()) {
            final ObjetMetier objet = new ObjetMetier(in.nextInt(), "nom", "descr", 0);
            listeObjetsMetier.add(objet);
            idObject++;
        }
        in.close();
        return listeObjetsMetier;
    }
}
