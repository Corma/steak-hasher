package utils;

import java.util.logging.*;

public class CustomLogger {

    public static void logInfo(String msg) {

        Logger logger = getLogger();
        logger.info(msg);
    }

    public static void logWarn(String msg) {

        Logger logger = getLogger();
        logger.warning(msg);
    }

    private static Logger getLogger() {
        Logger logger = Logger.getLogger(CustomLogger.class.getName());
        logger.setUseParentHandlers(false);

        CustomFormatter formatter = new CustomFormatter();
        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(formatter);

        logger.addHandler(handler);
        return logger;
    }
}