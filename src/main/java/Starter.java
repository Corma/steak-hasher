import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import model.ObjetMetier;
import model.ObjetMetierSortie;
import utils.InputReader;
import utils.OutputWriter;

public class Starter {
    public static void main(final String[] args) throws URISyntaxException, IOException {
        final String inputFileName = "input.in";
        final List<ObjetMetier> listeObjetsMetier = InputReader.getObjetsMetiersFromFile(inputFileName);

        for (final ObjetMetier objet : listeObjetsMetier) {
            System.out.println(objet);
        }
        final String outputFileName = inputFileName.replace("in", "out");
        final List<ObjetMetierSortie> listeObjetsMetierSortie = new ArrayList<>();
        OutputWriter.genereOutput(listeObjetsMetierSortie, outputFileName);
    }
}
