package model;

public final class ObjetMetierSortie {

    private final int id ;
    private final String nom;
    private final String description;
    private final int nombre;

    public ObjetMetierSortie(int id, String nom, String description, int nombre){
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.nombre = nombre;
    }


    int getId() {
        return id;
    }

    String getNom() {
        return nom;
    }

    String getDescription() {
        return description;
    }

    int getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return "ObjetMetier{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", description='" + description + '\'' +
                ", nombre=" + nombre +
                '}';
    }
}
