package model;

import model.ObjetMetier;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class objetMetierShould {


    @Test
     void returnValuesThatAreSetted (){

        ObjetMetier objet = new ObjetMetier(1,"leNom","LaDescr", 2);
        assertThat(objet.getId(),is(1));
        assertThat(objet.getDescription(),is("LaDescr"));
        assertThat(objet.getNom(),is("leNom"));
        assertThat(objet.getNombre(),is(2));

    }
}